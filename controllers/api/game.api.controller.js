const db = require('../../models')
const Game = db.games

module.exports = {
  getGame: async (req, res) => {
    try {
      const gid = parseInt(req.params.id, 10)
      const game = await Game.findByPk(gid)
      if (!game) {
        return res.status(400).json({
          result: 'ERROR',
          msg: 'Error occured while retrieving data'
        })
      }
      res.status(200).json({
        result: 'Success retrieving data',
        // eslint-disable-next-line object-shorthand
        game: game
      })
    } catch (err) {
      console.log(err)
    }
  },
  getAllGames: async (req, res) => {
    try {
      const games = await Game.findAll({})
      res.status(200).json({
        result: 'Success retrieving data',
        // eslint-disable-next-line object-shorthand
        games: games
      })
    } catch (err) {
      console.log(err)
    }
  }
}
