/* eslint-disable object-shorthand */
/* eslint-disable no-unused-vars */
const db = require('../../models')
const Score = db.scores
const User = db.users
const Game = db.games

module.exports = {
  addScore: async (req, res) => {
    const userId = req.params.id
    const gameId = req.params.gameId
    try {
      const user = await User.findByPk(userId)
      if (user) {
        const score = await Score.findOne({ where: { userId: userId, gameId: gameId } })
        if (score) {
          await Score.update(
            { score: score.score + 10 },
            { where: { userId: userId, gameId: gameId } }
          )
          return res.status(200).json({
            result: 'SUCCESS',
            msg: 'Score updated!'
          })
        } else {
          await Score.create({
            userId: userId,
            gameId: gameId,
            score: 10
          })
          return res.status(201).json({
            result: 'SUCCESS',
            msg: 'Score updated!'
          })
        }
      } else {
        return res.status(404).json({
          result: 'ERROR',
          msg: 'User not found'
        })
      }
    } catch (err) {
      console.log(err)
      // Send an error response
      res.status(500).json({ error: 'Internal server error' })
    }
  },
  getScore: async (req, res) => {
    const userId = req.params.id
    const score = await Score.findAll({
      where: {
        userId: userId
      },
      include: {
        model: User,
        attributes: ['username']
      }
    })
    res.status(200).json({
      result: 'Success',
      score: score
    })
  },
  getAllScore: async (req, res) => {
    try {
      const scores = await Score.findAll({
        include: {
          model: User,
          attributes: ['username']
        }
      })
      res.status(200).json({
        result: 'Success',
        scores: scores
      })
    } catch (err) {
      console.log(err)
    }
  },
  Leaderboard: async (req, res) => {
    const gameId = req.params.gameId
    await Score.findAll({
      where: { gameId: gameId },
      order: [['score', 'DESC']],
      include: {
        model: User,
        attributes: ['username']
      }
    })
      .then(leaderboard => {
        res.status(200).json({
          message: 'User Leaderboard',
          data: leaderboard
        })
      })
      .catch(error => {
        console.log(error)
        res.status(500).json({
          message: 'Error occurred while fetching leaderboard',
          error: error
        })
      })
  }
}
