const db = require('../models')
const Score = db.scores

const scoreSeedData = [
  {
    userId: 1,
    gameId: 1,
    score: 10
  },
  {
    userId: 1,
    gameId: 2,
    score: 20
  }
]

async function seedScore () {
  try {
    await Score.bulkCreate(scoreSeedData)

    console.log('Score seeded successfully.')
  } catch (error) {
    console.error('Error seeding score:', error)
  }
}

module.exports = seedScore
