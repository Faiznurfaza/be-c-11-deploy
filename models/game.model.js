module.exports = (sequelize, DataTypes) => {
  const Game = sequelize.define('Game', {
    title: {
      type: DataTypes.STRING,
      allowNull: true
    },
    description: {
      type: DataTypes.STRING,
      allowNull: true
    },
    thumbnail: {
      type: DataTypes.STRING,
      allowNull: true
    },
    play_count: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    how_to_play: {
      type: DataTypes.STRING,
      allowNull: true
    },
    release_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    latest_update: {
      type: DataTypes.STRING,
      allowNull: true
    }
  })
  return Game
}
